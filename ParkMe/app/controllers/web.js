/*
This is just a simple screen with a web view of the statistics website.
Author: Aleks Zuchowicz
Revision: 16/10/2015
*/

var args = arguments[0] || {};

$.webview.scalesPageToFit = true;

function goback(e) {
	$.web.close({activityExitAnimation: Ti.Android.R.anim.slide_out_right});
};