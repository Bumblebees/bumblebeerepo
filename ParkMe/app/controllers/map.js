/*
This is the code associated with the campus map screen. The entire window is filled with a map view.
An options view is toggled on or off and drawn over the map when a button in the action bar is pressed.
All annotations/polygons are created and stored in an array. Data is taken from alloy.js
Author: Aleks Zuchowicz
Revision: 16/10/2015
*/

//VARIABLES
var args = arguments[0] || {};
var MapModule = require('ti.map');

//create map
var map1 = MapModule.createView({
	userLocation: true,
	mapType: Ti.App.Properties.getInt("mapType"),
	region: {latitude: -37.7207614, longitude: 145.0490559, latitudeDelta: 0.012, longitudeDelta: 0.012 },
	top: "0dp"
});

//create car park annotations
var annotCarParksArray = [];
for(var i = 0, j = carParks.length; i<j; i++) {
	var annotCarPark = MapModule.createAnnotation({
		latitude: carParks[i].latitude,
		longitude: carParks[i].longitude,
		image: "images/maps/spot_annot.png",
		title: carParks[i].name,
		subtitle: "Capacity: " + carParks[i].capacity,
		centerOffset: {x: 0, y: -40}
	});
	annotCarParksArray.push(annotCarPark);
}

//create polygons for carparks
var polyCarParksArray = [];
for(var i=0,j=carParks.length; i<j; i++) {
	var carParkPolygon = MapModule.createPolygon({
		fillColor: "#3FFF7043", //25% opacity deep orange 400
		strokeColor: "#FF5722",
		strokeWidth: 4,
		points: carParks[i].area
	});
	polyCarParksArray.push(carParkPolygon);
}

//create ticket machine annotations
var annotTicketsArray = [];
for(var i = 0, j = tmLocations.length; i<j; i++) {
	var annotTicket = MapModule.createAnnotation({
		latitude: tmLocations[i].latitude,
		longitude: tmLocations[i].longitude,
		image: "images/maps/ticket_machine_annot.png",
		title: "Ticket Machine",
		subtitle: tmLocations[i].carPark,
		centerOffset: {x: 0, y: -40}
	});
	annotTicketsArray.push(annotTicket);
}

//create building annotations
var annotBuildingsArray = [];
for(var i = 0, j = academicBuildings.length; i<j; i++) {
	var annotBuilding = MapModule.createAnnotation({
		latitude: academicBuildings[i].latitude,
		longitude: academicBuildings[i].longitude,
		image: "images/maps/building_annot.png",
		title: academicBuildings[i].name,
		subtitle: academicBuildings[i].id,
		centerOffset: {x: 0, y: -40}
	});
	annotBuildingsArray.push(annotBuilding);
}

//create motorbike parking annotations
var annotMotorbikesArray = [];
for(var i = 0, j = motorbikeLocations.length; i<j; i++) {
	var annotMotorbike = MapModule.createAnnotation({
		latitude: motorbikeLocations[i].latitude,
		longitude: motorbikeLocations[i].longitude,
		image: "images/maps/motorbike_annot.png",
		title: motorbikeLocations[i].name,
		subtitle: "Capacity: " + motorbikeLocations[i].capacity,
		centerOffset: {x: 0, y: -40}
	});
	annotMotorbikesArray.push(annotMotorbike);
}

//FUNCTIONS
//the following functions add or remove their annotations depending on the value of the switch
function carParkSwitch(e) {
	if(e.value) {
		map1.addAnnotations(annotCarParksArray);
		//map1.addPolygons(polyCarParksArray); doesn't work
		for(var i = 0, j = polyCarParksArray.length; i<j; i++) {
  			map1.addPolygon(polyCarParksArray[i]);
		}
	}	
	else {
		map1.removeAnnotations(annotCarParksArray);
		//map1.removePolygons(polyCarParksArray); doesn't work
		for(var i = 0, j = polyCarParksArray.length; i<j; i++) {
  			map1.removePolygon(polyCarParksArray[i]);
		}
	}
}

function ticketSwitch(e) {
	if(e.value) {
		map1.addAnnotations(annotTicketsArray);
	}	
	else {
		map1.removeAnnotations(annotTicketsArray);
	}
}

function buildingSwitch(e) {
	if(e.value) {
		map1.addAnnotations(annotBuildingsArray);
	}	
	else {
		map1.removeAnnotations(annotBuildingsArray);
	}
}

function motorbikeSwitch(e) {
	if(e.value) {
		map1.addAnnotations(annotMotorbikesArray);
	}	
	else {
		map1.removeAnnotations(annotMotorbikesArray);
	}
}

var optionsVisible = false;
//funtion to open options view
function openAdd(e) {
	if(optionsVisible) {
		$.map.remove($.legendView);
		optionsVisible = false;
	}
	else {
		$.map.add($.legendView);
		optionsVisible = true;
	}
}

function goback(e) {
	$.map.close({activityExitAnimation: Ti.Android.R.anim.fade_out});
}

//INIT
$.map.add(map1);
map1.addAnnotations(annotCarParksArray);
//map1.addPolygons(polyCarParksArray); doesn't work
for(var i = 0, j = polyCarParksArray.length; i<j; i++) {
  map1.addPolygon(polyCarParksArray[i]);
}
//map1.addAnnotations(annotBuildingsArray); start off
map1.addAnnotations(annotTicketsArray);
map1.addAnnotations(annotMotorbikesArray);
$.map.remove($.legendView); //start with legend removed from view
