/*
This screen displays a list view containg all the buildings defined in alloy.js.
For android the search view is contained in the action bar
For iOS the search view is a propery of the list view (defined in the xml)
Author: Aleks Zuchowicz
Revision: 16/10/2015
*/

//VARIABLES
var args = arguments[0] || {};
var	buildingListItems = [];
var favourites = Ti.App.Properties.getList("favourites");

//add the search view to the actionbar on android
if(OS_ANDROID) {
	var search = Ti.UI.Android.createSearchView({
		hintText: "Search Buildings",
		color: "#FAFAFA"
	});
	$.buildingList.searchView = search;
	$.buildinglist.activity.onCreateOptionsMenu = function(e) {
		e.menu.add({
			actionView: search,
			showAsAction: Ti.Android.SHOW_AS_ACTION_ALWAYS
		});
	};
}

//Ti.App.Properties.setList("favourites", []);

//FUNCTIONS
//creates a list item for every building in the building array
function refreshList() {	
	//Fill in favourites section
	for(var i = 0; i < favourites.length; i++) {
		var buildingIndex = -1;
		for(var j = 0; j < academicBuildings.length; j++) {
	  		if(favourites[i] == academicBuildings[j].id) {
	  			buildingIndex = j;
	  			break;
	  		}
		};
		var favNewRow = [({  
	  		leftLabel: {text: academicBuildings[buildingIndex].name},
	  		star: {image: "/images/icons/ic_star_black_36dp.png"}, 
	  		rightLabel: {text: favourites[i]},
	  		properties: {
	  			itemId: academicBuildings[buildingIndex].id,
	  			height: "50dp",
	  			searchableText: academicBuildings[buildingIndex].name + " " + academicBuildings[buildingIndex].id, 
	  			accessoryType: Ti.UI.LIST_ACCESSORY_TYPE_DISCLOSURE,
	  			selectedBackgroundColor: "#0091EA"
	  		}
	  	})];
	  	$.favSection.appendItems(favNewRow);
	};
	
	//Fill in building list
	buildingListItems = [];
	for(var i=0,j=academicBuildings.length; i<j; i++){  	
	  	var starImage = "/images/icons/ic_star_border_black_36dp.png";
	  	if(favourites.indexOf(academicBuildings[i].id) != -1){
	  		starImage = "/images/icons/ic_star_black_36dp.png";
	  	}
	  	var newRow = [({  
	  		leftLabel: {text: academicBuildings[i].name},
	  		star: {image: starImage}, 
	  		rightLabel: {text: academicBuildings[i].id},
	  		properties: {
	  			itemId: academicBuildings[i].id,
	  			height: "50dp",
	  			searchableText: academicBuildings[i].name + " " + academicBuildings[i].id, 
	  			accessoryType: Ti.UI.LIST_ACCESSORY_TYPE_DISCLOSURE,
	  			selectedBackgroundColor: "#0091EA"
	  		}
	  	})];
	  		
	  	switch(academicBuildings[i].name.charAt(0)){
			case "A": $.sectionA.appendItems(newRow);
					break;
			case "B": $.sectionB.appendItems(newRow);
					break;
			case "C": $.sectionC.appendItems(newRow);
					break;
			case "D": $.sectionD.appendItems(newRow);
					break;		
			case "E": $.sectionE.appendItems(newRow);
					break;
			case "F": $.sectionF.appendItems(newRow);
					break;
			case "G": $.sectionG.appendItems(newRow);
					break;
			case "H": $.sectionH.appendItems(newRow);
					break;
			case "I": $.sectionI.appendItems(newRow);
					break;
			case "J": $.sectionJ.appendItems(newRow);
					break;
			case "K": $.sectionK.appendItems(newRow);
					break;
			case "L": $.sectionL.appendItems(newRow);
					break;
			case "M": $.sectionM.appendItems(newRow);
					break;
			case "N": $.sectionN.appendItems(newRow);
					break;		
			case "O": $.sectionO.appendItems(newRow);
					break;
			case "P": $.sectionP.appendItems(newRow);
					break;
			case "Q": $.sectionQ.appendItems(newRow);
					break;
			case "R": $.sectionR.appendItems(newRow);
					break;
			case "S": $.sectionS.appendItems(newRow);
					break;
			case "T": $.sectionT.appendItems(newRow);
					break;
			case "U": $.sectionU.appendItems(newRow);
					break;
			case "V": $.sectionV.appendItems(newRow);
					break;
			case "W": $.sectionW.appendItems(newRow);
					break;
			case "X": $.sectionX.appendItems(newRow);
					break;
			case "Y": $.sectionY.appendItems(newRow);
					break;
			case "Z": $.sectionZ.appendItems(newRow);
					break;
			default: $.sectionA.appendItems(newRow);
		}	
	};
};

//This function is called when an item in the list view is clicked. 
//It grabs the building id of the clicked item and opens the navigation screen with that param.
function listItemClicked(e) {
	var section = $.buildingList.sections[e.sectionIndex];
	var item = section.getItemAt(e.itemIndex);
	Ti.API.info(item.properties.itemId);
	Ti.API.info(e.source.toString());
	Ti.API.info(e.bindId);
	if(e.bindId == "star") {
		//star clicked
		if(favourites.indexOf(item.properties.itemId) == -1) {
			//add to favourites
			favourites.push(item.properties.itemId);
			item.star.image = "/images/icons/ic_star_black_36dp.png";
			$.favSection.appendItems([item]);
		}
		else {
			//remove from favourites
			//favourites.splice(favourites.indexOf(item.properties.itemId), 1);
			
			item.star.image = "/images/icons/ic_star_border_black_36dp.png";
			Ti.API.info(favourites.indexOf(item.properties.itemId));
			$.favSection.deleteItemsAt(favourites.indexOf(item.properties.itemId), 1);
			favourites = _.without(favourites, item.properties.itemId);
		}
		if(e.sectionIndex != 0) {
			section.updateItemAt(e.itemIndex, item); //update item
		}
		else {
			var allSections = $.buildingList.getSections();
			for(var i=0; i < allSections.length; i++) {
			  	for(var j = 0; j < allSections[i].getItems().length; j++){
					if(allSections[i].getItemAt(j).properties.itemId == item.properties.itemId) {
						Ti.API.info("yay");
						//allSections[i].getItemAt(j).star.image = "/images/icons/ic_star_border_white_48dp.png";
						allSections[i].updateItemAt(j, item);
					}
			  	}
			  
			}
			//section.updateItemAt(section.getItemAt(), item)	
		}
		Ti.API.info(favourites);
		Ti.App.Properties.setList("favourites", favourites);
	}
	else {
		//row clicked
		var navController = Alloy.createController("navigation", [item.properties.itemId, null, null]).getView();
		if(OS_IOS) {
			navWin.openWindow(navController);
		}
		if(OS_ANDROID) {
			navController.open({activityEnterAnimation: Ti.Android.R.anim.fade_in, activityExitAnimation: Ti.Android.R.anim.fade_out});
		}
	}
}

function test(e) {
	Ti.API.info("test");
}

function goback(e) {
	$.buildinglist.close({activityExitAnimation: Ti.Android.R.anim.fade_out});
}

//INIT
refreshList();