/*
This controller is responsible for all the calculations involved with picking a car spot.
It is also responsible for displaying a map view with the chosen spot annotated as well as building and nearest ticket machine if applicable.
It has buttons that allow the user to change the chosen row further or closer.
A navigate me button to launch turn by turn navigation.  
Author: Aleks Zuchowicz
Revision: 16/10/2015
*/

//VARIABLES
var args = arguments[0] || {};
var MapModule = require('ti.map');
var index = 0; //the selected row in the distances array 
var buildingIndex = null; //the selected row 
var carParkIndex = null;
var distances = [];
var tmDistances = [];

var annotBuilding = null;
var routeBuilding = null;

var agora = {latitude: -37.720728, longitude: 145.048398};

//Finds the index of the passed in building id or carPark id
if(args[0] != null && args[1] == null && args[2] == null) { 
	for(var i=0, j = academicBuildings.length; i<j; i++){
	  	if(args[0] == academicBuildings[i].id) {
	  		buildingIndex = i;
	  		break;
	  	}
	};
	calculateDistances(academicBuildings[buildingIndex], null); //by building
}
else if(args[0] == null && args[1] != null && args[2] == null) { //first arg was null, must be carPark id
	for(var i=0, j = carParks.length; i<j; i++){
	  	if(args[1] == carParks[i].id) {
			calculateDistances(agora, carParks[i].id); //by car park
	  		break;
	  	}
	};	
}
else {
	calculateDistances(agora, null); //quick park: find nearest spot to agora
}
calculateTMDistances(); //Ticket machine distances

var map1 = MapModule.createView({
	userLocation: true,
	mapType: Ti.App.Properties.getInt("mapType"),
	height: "64%",
	width: "98%",
	bottom: "17%"
});


var annotSpot = MapModule.createAnnotation({
	image: "images/maps/spot_annot.png",
	title: "Your Spot is here!",
	centerOffset: {x: 0, y: -40}
});
	
var annotTickets = MapModule.createAnnotation({
	image: "images/maps/ticket_machine_annot.png",
	title: "Ticket Machine",
	centerOffset: {x: 0, y: -40}
});

var routeTickets = MapModule.createRoute({
	width: 4,
	color: "#2196F3",
	points: [
		{latitude: distances[index].latitude, longitude: distances[index].longitude}, 
		{latitude: tmDistances[0].latitude, longitude: tmDistances[0].longitude}
	]
});

//FUNCTIONS

//Equirectangular approximation to calulate distance in meters between 2 points on earth
function calculateDistance(lat1, lat2, long1, long2) {
	var R = 6371000; // mean radius of the earth in meters
	var lat1 = toRadians(lat1);
	var lat2 = toRadians(lat2);
	var long1 = toRadians(long1);
	var long2 = toRadians(long2);
	
	var x = (long2 - long1) * Math.cos((lat1 + lat2) / 2);
	var y = (lat2 - lat1);
	var distance = Math.round(Math.sqrt(x * x + y * y) * R);
	
	return distance;
};

//convert degrees to radians
function toRadians(x) {
	return x * Math.PI / 180;
};

//launch Apple Maps or Google Maps with directions
function openTurnByTurn() {
	if(OS_IOS) {
		//Ti.Platform.openURL("Maps://?saddr=" + coordBuilding.latitude + "," + coordBuilding.longitude + "&" + "daddr=" + coordSpot.latitude + "," + coordSpot.longitude);
		Ti.Platform.openURL("Maps://?saddr=Current%20Location&daddr=" + distances[0].latitude + "," + distances[0].longitude);
		//Ti.Platform.openURL("comgooglemaps://?saddr=Current%20Location&daddr=" + distances[0].latitude + "," + distances[0].longitude);
	}
	if(OS_ANDROID) {
		var navIntent = Ti.Android.createIntent({
			action: Ti.Android.ACTION_VIEW,
			data: "google.navigation:q=" + distances[0].latitude + "," + distances[0].longitude
		});
		Ti.Android.currentActivity.startActivity(navIntent);
	}
};

function prevRow(e) {
	if(index > 0) {
		index--;
		redraw();
	}
}

function nextRow(e) {
	if(index < distances.length - 1) {
		index++;
		redraw();
	}	
}

function goback(e) {
	$.navigation.close({activityExitAnimation: Ti.Android.R.anim.fade_out});
};

//updates all the annotations and routes with then newly selected row
function redraw() {
	tmDistances = [];
	calculateTMDistances();
	
	annotSpot.setLatitude(distances[index].latitude);
	annotSpot.setLongitude(distances[index].longitude);
	annotSpot.setSubtitle(distances[index].carPark + " " + distances[index].id);
	annotTickets.setLatitude(tmDistances[0].latitude);
	annotTickets.setLongitude(tmDistances[0].longitude);
	annotTickets.setSubtitle(tmDistances[0].distance + "m");
	
	if(buildingIndex != null) {
		annotBuilding.setLatitude(academicBuildings[buildingIndex].latitude);
		annotBuilding.setLongitude(academicBuildings[buildingIndex].longitude);
		annotBuilding.setSubtitle(distances[index].distance + "m");
		//iOS does not support changing points on routes. This is a work around.
		if(OS_IOS) {
			map1.removeRoute(routeBuilding);	
			routeBuilding = MapModule.createRoute({
				width: 4,
				color: "#4CAF50",
				points: [
					{latitude: distances[index].latitude, longitude: distances[index].longitude}, 
					{latitude: academicBuildings[buildingIndex].latitude, longitude: academicBuildings[buildingIndex].longitude}
				]
			});
			map1.addRoute(routeBuilding);	
		}
		if(OS_ANDROID) {
			routeBuilding.setPoints([
				{latitude: distances[index].latitude, longitude: distances[index].longitude}, 
				{latitude: academicBuildings[buildingIndex].latitude, longitude: academicBuildings[buildingIndex].longitude}
			]);
		}
		$.buildingDistanceLabel.setText(distances[index].distance + "m to " + academicBuildings[buildingIndex].id);
	}
	
	//iOS does not support changing points on routes. This is a work around.	
	if(Ti.App.Properties.getBool("ticketMachineSwitch") && OS_IOS) {
		map1.removeRoute(routeTickets);	
		routeTickets = MapModule.createRoute({
			width: 4,
			color: "#2196F3",
			points: [
				{latitude: distances[index].latitude, longitude: distances[index].longitude}, 
				{latitude: tmDistances[0].latitude, longitude: tmDistances[0].longitude}
			]
		});
		map1.addRoute(routeTickets);
	}
	if(OS_ANDROID) {
		routeTickets.setPoints([
			{latitude: distances[index].latitude, longitude: distances[index].longitude}, 
			{latitude: tmDistances[0].latitude, longitude: tmDistances[0].longitude}
		]);
	}
			
	map1.setRegion({
		latitude: distances[index].latitude, 
		longitude: distances[index].longitude, 
		latitudeDelta: 0.005, 
		longitudeDelta: 0.005
	});
	//match the distances car park id with its name from carParks 
	for (var i=0; i < carParks.length; i++) {
		if(carParks[i].id == distances[index].carPark) {
			$.topLabel.setText(carParks[i].name);	
		}
	};
	$.bottomLabel.setText("Spots Available: " + distances[index].freeSpots);
}

//calculate nearest car spots to a locataion with optional car park filter
function calculateDistances(fromLocation, carParkFilter) {
	for(var i = 0, j = rowLocations.length; i<j; i++){
		if(rowLocations[i].carPark == carParkFilter || carParkFilter == null) {
			var lat1 = fromLocation.latitude;
			var lat2 = rowLocations[i].latitude;
			var long1 = fromLocation.longitude;
			var long2 = rowLocations[i].longitude;
		
			distances.push({
				id: rowLocations[i].id, 
				carPark: rowLocations[i].carPark,
				freeSpots: rowLocations[i].freeSpots, 
				latitude: rowLocations[i].latitude, 
				longitude: rowLocations[i].longitude, 
				distance: calculateDistance(lat1, lat2, long1, long2)
			});
		}
	}
	distances = _.sortBy(distances, function(o) {return o.distance;}); //sort by distance using an underscore.js function
}

//calculate nearest ticket machine dictances to car spot
function calculateTMDistances() {
	for(var i = 0, j = tmLocations.length; i<j; i++){
		var lat1 = distances[index].latitude;
		var lat2 = tmLocations[i].latitude;
		var long1 = distances[index].longitude;
		var long2 = tmLocations[i].longitude;
	
		tmDistances.push({
			carPark: tmLocations[i].carPark, 
			latitude: tmLocations[i].latitude, 
			longitude: tmLocations[i].longitude, 
			distance: calculateDistance(lat1, lat2, long1, long2)
		});
	}
	tmDistances = _.sortBy(tmDistances, function(o) {return o.distance;}); //sort by distance using an underscore.js function
}
	
//INIT

//this finds the best row with more than one available spot
if(distances[index].freeSpots != -1) { //if offline just find the nearest row
	while(distances[index].freeSpots < 1) {
		if(distances.length > index) {
			index++;
		}
		else {
			break;
		}
	}
}

map1.addAnnotation(annotSpot);

if(Ti.App.Properties.getBool("ticketMachineSwitch")) {
	map1.addAnnotation(annotTickets);
	map1.addRoute(routeTickets);
}

if(buildingIndex != null) {
	annotBuilding = MapModule.createAnnotation({
		image: "images/maps/building_annot.png",
		title: academicBuildings[buildingIndex].name,
		centerOffset: {x: 0, y: -40}
	});
	routeBuilding = MapModule.createRoute({
		width: 4,
		color: "#4CAF50",
		points: [
			{latitude: distances[index].latitude, longitude: distances[index].longitude}, 
			{latitude: academicBuildings[buildingIndex].latitude, longitude: academicBuildings[buildingIndex].longitude}
		]
	});
	map1.addAnnotation(annotBuilding);
	map1.addRoute(routeBuilding);
}

redraw();
$.navigation.add(map1);