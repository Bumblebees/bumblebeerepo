/*
This screen displays a table view.
There is also an entry in the table to open a web view in a new screen with the statistics website.
Author: Aleks Zuchowicz
Revision: 16/10/2015
*/

//VARIABLES
var args = arguments[0] || {};
var MapModule = require('ti.map');

//FUNCTIONS
function openStats() {
	/*
	var webController = Alloy.createController("web").getView();
	if(OS_IOS) {
		navWin.openWindow(webController);
	}
	if(OS_ANDROID) {
		webController.open({activityEnterAnimation: Ti.Android.R.anim.slide_in_left, activityExitAnimation: Ti.Android.R.anim.slide_out_right});
	}
	*/
	Ti.Platform.openURL("http://homepage.cs.latrobe.edu.au/15pra02/logFilesWebPage/index.php");	
}

function goback(e) {
	$.settings.close({activityExitAnimation: Ti.Android.R.anim.fade_out});
}

//called when any switch changes value
function saveSettings() {
    Ti.App.Properties.setBool("ticketMachineSwitch", $.ticketMachineSwitch.getValue());
}

//INIT

//save the map type picker setting 
//iOS does not have a Terrain type map and a tabbed bar is used instead of a picker
if(OS_ANDROID) {
	$.mapTypePicker.addEventListener("change", function(e) {
		Ti.API.info(e.row.getTitle());
		switch(e.row.getTitle()) {
			case "Normal": Ti.App.Properties.setInt("mapType", MapModule.NORMAL_TYPE);
				break;
			case "Satellite": Ti.App.Properties.setInt("mapType", MapModule.SATELLITE_TYPE);
				break;
			case "Terrain": Ti.App.Properties.setInt("mapType", MapModule.TERRAIN_TYPE);
				break;
			case "Hybrid": Ti.App.Properties.setInt("mapType", MapModule.HYBRID_TYPE);
				break;
			default: Ti.App.Properties.setInt("mapType", MapModule.NORMAL_TYPE);
		}	
	});
}

if(OS_IOS) {
	$.mapTypeTabbedBar.addEventListener("click", function(e) { 
		Ti.API.info(e.index);
		switch(e.index) {
			case 0: Ti.App.Properties.setInt("mapType", MapModule.NORMAL_TYPE);
				break;
			case 1: Ti.App.Properties.setInt("mapType", MapModule.SATELLITE_TYPE);
				break;
			case 2: Ti.App.Properties.setInt("mapType", MapModule.HYBRID_TYPE);
				break;
			default: Ti.App.Properties.setInt("mapType", MapModule.NORMAL_TYPE);
		}		
	});
}

//load setting values to display correct switch and picker values	
$.ticketMachineSwitch.setValue(Ti.App.Properties.getBool("ticketMachineSwitch"));
if(OS_ANDROID) {
	switch(Ti.App.Properties.getInt("mapType")) {
		case MapModule.NORMAL_TYPE: $.mapTypePicker.setSelectedRow(0, 0, false);
			break;
		case MapModule.SATELLITE_TYPE: $.mapTypePicker.setSelectedRow(0, 1, false);
			break;
		case MapModule.TERRAIN_TYPE: $.mapTypePicker.setSelectedRow(0, 2, false);
			break;
		case MapModule.HYBRID_TYPE: $.mapTypePicker.setSelectedRow(0, 3, false);
			break;
		default: $.mapTypePicker.setSelectedRow(0, 0, false);
	}
}
if(OS_IOS) {
	switch(Ti.App.Properties.getInt("mapType")) {
		case MapModule.NORMAL_TYPE: $.mapTypeTabbedBar.setIndex(0);
			break;
		case MapModule.SATELLITE_TYPE: $.mapTypeTabbedBar.setIndex(1);
			break;
		case MapModule.HYBRID_TYPE: $.mapTypeTabbedBar.setIndex(2);
			break;
		default: $.mapTypeButtonBar.setIndex(0);
	}
}