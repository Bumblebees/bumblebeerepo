/*
This is the code associated with the info screen. This screen contains 4 tabs to categorise the information.
Author: Kylie Marshall
Revision: 16/10/2015
*/

var args = arguments[0] || {};

function goback(e) {
	$.info.close({activityExitAnimation: Ti.Android.R.anim.fade_out});
}
function openURL(e){
	Titanium.Platform.openURL('http://www.latrobe.edu.au/transport-central/car-parking/melbourne-parking/fees');
}
function openMap(e) {
	var mapController = Alloy.createController("map").getView();
	if(OS_IOS) {
		navWin.openWindow(mapController);
	}
	if(OS_ANDROID) {
		mapController.open();
	}
}

