/*
This is the code associated with the index screen. It is the first screen that is loaded when the app starts.
The top half of the screen is a list view which contains all the car parks and the number of available spots.
The bottom half contains buttons to open: the navigation page, building list, info page, campus map or settings.
Author: Aleks Zuchowicz
Revision: 16/10/2015
*/

//VARIABLES
var quickLinksItems = [];
var MapModule = require('ti.map');

//FUNCTIONS
//The following functions open other screens
function openMap(e) {
	var mapController = Alloy.createController("map").getView();
	if(OS_IOS) {
		navWin.openWindow(mapController);
	}
	if(OS_ANDROID) {
		mapController.open({activityEnterAnimation: Ti.Android.R.anim.fade_in, activityExitAnimation: Ti.Android.R.anim.fade_out});
	}
}

function autoChooseSelected() {
	//navigation page takes 3 parameters: building ID, car park ID and "auto"
	//use only one paramenter and set the others to null
	var navController = Alloy.createController("navigation", [null, null, "auto"]).getView();
	if(OS_IOS) {
		navWin.openWindow(navController);
	}
	if(OS_ANDROID) {
		navController.open({activityEnterAnimation: Ti.Android.R.anim.fade_in, activityExitAnimation: Ti.Android.R.anim.fade_out});
	}	
}

function openSelection(e) {
	var selectionController = Alloy.createController("buildinglist").getView();
	if(OS_IOS) {
		navWin.openWindow(selectionController);
	}
	if(OS_ANDROID) {
		selectionController.open({activityEnterAnimation: Ti.Android.R.anim.fade_in, activityExitAnimation: Ti.Android.R.anim.fade_out});
	}
}

function openNavigation(e) {
	var navController = Alloy.createController("navigation").getView();
	if(OS_IOS) {
		navWin.openWindow(navController);
	}
	if(OS_ANDROID) {
		navController.open({activityEnterAnimation: Ti.Android.R.anim.fade_in, activityExitAnimation: Ti.Android.R.anim.fade_out});
	}
}

function openInfo(e) {
	var infoController = Alloy.createController("info").getView();
	if(OS_IOS) {
		navWin.openWindow(infoController);
	}
	if(OS_ANDROID) {
		infoController.open({activityEnterAnimation: Ti.Android.R.anim.fade_in, activityExitAnimation: Ti.Android.R.anim.fade_out});
	}
}
function openSettings(e) {
	var settingsController = Alloy.createController("settings").getView();
	if(OS_IOS) {
		navWin.openWindow(settingsController);
	}
	if(OS_ANDROID) {
		settingsController.open({activityEnterAnimation: Ti.Android.R.anim.fade_in, activityExitAnimation: Ti.Android.R.anim.fade_out});
	}
}

//This function is called when an item in the list view is clicked. 
//It grabs the car park id of the clicked item and opens the navigation screen with that param.
function listItemClicked(e) {
	var section = $.quickLinks.sections[e.sectionIndex];
	var item = section.getItemAt(e.itemIndex);
	Ti.API.info(item.properties.itemId);
	Ti.API.info(item.properties.itemFreeSpots);
	if(item.properties.itemFreeSpots > 0) {
		var navController = Alloy.createController("navigation", [null, item.properties.itemId, null]).getView();
		if(OS_IOS) {
			navWin.openWindow(navController);
		}
		if(OS_ANDROID) {
			navController.open({activityEnterAnimation: Ti.Android.R.anim.fade_in, activityExitAnimation: Ti.Android.R.anim.fade_out});
		}
	}
	else {
		//Do not open the navigation screen if there are no spots!
		alert("There are no car spots remaining in this Car Park!");
	}
}

//refreshes the list with the new data received from the server
function refreshList() {
	quickLinksItems = [];
	for(var i=0,j=carParks.length; i<j; i++){
  		quickLinksItems.push({
  			leftLabel: {text: carParks[i].name}, 
  			rightLabel: {text: carParks[i].freeSpots},
  			properties: {
  				itemId: carParks[i].id,
  				itemFreeSpots: carParks[i].freeSpots,
  				searchableText: carParks[i].name, 
  				height: "45dp",
  				accessoryType: Ti.UI.LIST_ACCESSORY_TYPE_DISCLOSURE,
  				selectedBackgroundColor: "#0091EA"
  			}
  		});
	};
	$.quickLinks.sections[0].setItems(quickLinksItems);
};

//Updates the last refreshed time and date
function getDate() {
    var currentDate = new Date();
    $.time.setText("Updated on "+ String.formatDate(currentDate) + " at " + String.formatTime(currentDate)); 
};       
        
function getCarParkFreeSpots(e) { 
    //function to use HTTP to connect to a web server and transfer the data. 
	var httpClient = Ti.Network.createHTTPClient({ 
    	onerror: function(e) { 
			Ti.API.debug(e.error); 
        	alert('There was an error during the connection!'); 
         }, 
         timeout:1000, 
    });
                            
    httpClient.open('GET', 'http://homepage.cs.latrobe.edu.au/15pra02/getFreeSpots.php');
    httpClient.send(); 
    
    //Function to be called upon a successful response 
    httpClient.onload = function() { 
    	var json = JSON.parse(this.responseText); 
        var json = json.CarParks;                                 
		
		//match the car park id in the json to those in alloy.js 
        for(var i = 0; i < json.length; i++) {        
        	for(var j = 0; j < carParks.length; j++) {
			  	if(carParks[j].id == json[i].id) {
			  		carParks[j].freeSpots = json[i].freeSpots;
			  	}
			};                
        };
        //carParks[1].freeSpots = 0; //use this to override database
        refreshList();
        getRowFreeSpots();        
        getDate();                                  
	}; 
};

function getRowFreeSpots(e) { 
    //function to use HTTP to connect to a web server and transfer the data. 
	var httpClient = Ti.Network.createHTTPClient({ 
    	onerror: function(e) { 
			Ti.API.debug(e.error); 
        	alert('There was an error during the connection!'); 
         }, 
         timeout:1000, 
    });
                            
    httpClient.open('GET', 'http://homepage.cs.latrobe.edu.au/15pra02/rowAvailable.php');
    httpClient.send(); 
    
    //Function to be called upon a successful response 
    httpClient.onload = function() { 
    	var json = JSON.parse(this.responseText); 
        var json = json.CarParks;                                 
		
		//match the row id in the json to those in alloy.js 
        for(var i = 0; i < json.length; i++) {        
        	for(var j = 0; j < rowLocations.length; j++) {
			  	if(rowLocations[j].id == json[i].id) {
			  		rowLocations[j].freeSpots = json[i].freeSpots;
			  	}
			};                
        };                                  
	}; 
};

//INIT
getCarParkFreeSpots();
refreshList(); //to populate quicklinks when there is no internet

//init default settings for first run of the app
if(Ti.App.Properties.getBool("firstRun") == null) {
	Ti.App.Properties.setBool("firstRun", false);
	Ti.App.Properties.setBool("ticketMachineSwitch", true);
	Ti.App.Properties.setInt("mapType", MapModule.HYBRID_TYPE);
	Ti.App.Properties.setList("favourites", []);
}

if(OS_IOS) {
	navWin.setWindow($.index);
	navWin.open();
}
if(OS_ANDROID) {
	$.index.open(); //open the window
}