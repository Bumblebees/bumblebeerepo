// The contents of this file will be executed before any of
// your view controllers are ever executed, including the index.
// You have access to all functionality on the `Alloy` namespace.
//
// This is a great place to do any initialization for your app
// or create any global variables/functions that you'd like to
// make available throughout your app. You can easily make things
// accessible globally by attaching them to the `Alloy.Globals`
// object. For example:
//
// Alloy.Globals.someGlobalFunction = function(){};
/*
All data regarding the car parks, car park rows and buildings are stored here.
The user's settings are also stored here.
All the variables/functions defined here are accessable everywhere.
Author: Aleks Zuchowicz, Kylie Marshall
Revision: 16/10/2015
*/

Alloy.Globals.Map = require('ti.map');

//iOS's navigation controller to switch back and forth through windows
var navWin = null; //out here to make it global
if(OS_IOS) {
	navWin = Ti.UI.iOS.createNavigationWindow({tintColor: "white"}); //tint didnt work when placed in app.tss
}

//SETTINGS
var ticketMachineSwitch = Ti.App.Properties.getBool("ticketMachineSwitch");
var mapType = Ti.App.Properties.getInt("mapType");

//DATA
var carParks = [
	{id: "CP1", name: "Car Park 1", freeSpots: -1, capacity: 211, latitude: -37.720067, longitude: 145.044829, area: [
		{latitude: -37.721144, longitude: 145.044501},
        {latitude: -37.720212, longitude: 145.044473},
        {latitude: -37.719034, longitude: 145.044783},
        {latitude: -37.719138, longitude: 145.045263},
        {latitude: -37.720468, longitude: 145.045302},
        {latitude: -37.721083, longitude: 145.045355}]
    }, 
	{id: "CP2", name: "Car Park 2", freeSpots: -1, capacity: 481, latitude: -37.722192, longitude: 145.045262, area: [
        {latitude: -37.723275, longitude: 145.045793},
        {latitude: -37.722602, longitude: 145.044935},
        {latitude: -37.721224, longitude: 145.04451},
        {latitude: -37.721138, longitude: 145.045185},
        {latitude: -37.722122, longitude: 145.04541},
        {latitude: -37.722006, longitude: 145.04603},
        {latitude: -37.722386, longitude: 145.046581}]
    }, 
   	{id: "CP2a", name: "Car Park 2a", freeSpots: -1, capacity: 178, latitude: -37.723574, longitude: 145.044330, area: [
        {latitude: -37.723299, longitude: 145.043815},
        {latitude: -37.723083, longitude: 145.044276},
        {latitude: -37.723896, longitude: 145.044866},
        {latitude: -37.724092, longitude: 145.044342},]
    }, 
	{id: "CP3", name: "Car Park 3", freeSpots: -1, capacity: 593, latitude: -37.724871, longitude: 145.050173, area: [
    	{latitude: -37.725131, longitude: 145.049072},
        {latitude: -37.725351, longitude: 145.050016},
        {latitude: -37.72514, longitude: 145.051301},
        {latitude: -37.724191, longitude: 145.050754},
        {latitude: -37.724388, longitude: 145.050084},
        {latitude: -37.724544, longitude: 145.048820},
        {latitude: -37.724916, longitude: 145.04887}]
    }, 
	{id: "CP4", name: "Car Park 4", freeSpots: -1, capacity: 353, latitude: -37.724380, longitude: 145.052247, area: [
        {latitude: -37.724091, longitude: 145.051068},
        {latitude: -37.723346, longitude: 145.052413},
        {latitude: -37.724676, longitude: 145.05317},
        {latitude: -37.725146, longitude: 145.051985}]
    }, 
	{id: "CP6", name: "Car Park 6", freeSpots: -1, capacity: 601, latitude: -37.720918, longitude: 145.054575, area: [
        {latitude: -37.720055, longitude: 145.053702},
        {latitude: -37.719967, longitude: 145.055045},
        {latitude: -37.721918, longitude: 145.055309},
        {latitude: -37.721656, longitude: 145.053961}]
    }, 
	{id: "CP7", name: "Car Park 7", freeSpots: -1, capacity: 710, latitude: -37.718073, longitude: 145.051121, area: [
    	{latitude: -37.717294, longitude: 145.05039},
        {latitude: -37.71754, longitude: 145.05106},
       	{latitude: -37.717707, longitude: 145.051175},
        {latitude: -37.717703, longitude: 145.051673},
        {latitude: -37.718372, longitude: 145.052534},
        {latitude: -37.718777, longitude: 145.051742},
        {latitude: -37.718751, longitude: 145.051027},
        {latitude: -37.718186, longitude: 145.050116}]
    }, 
	{id: "CP8", name: "Car Park 8", freeSpots: -1, capacity: 663, latitude: -37.717647, longitude: 145.049028, area: [
    	{latitude: -37.717121, longitude: 145.047992},
        {latitude: -37.717122, longitude: 145.049572},
        {latitude: -37.717293, longitude: 145.050278},
        {latitude: -37.718288, longitude: 145.04994},
        {latitude: -37.718191, longitude: 145.049192},
        {latitude: -37.718202, longitude: 145.048187},
        {latitude: -37.71785, longitude: 145.047873}]
    }, 
	{id: "CP9", name: "Car Park 9", freeSpots: -1, capacity: 169, latitude: -37.717591, longitude: 145.045847, area: [
    	{latitude: -37.717585, longitude: 145.045064},
        {latitude: -37.717248, longitude: 145.045584},
        {latitude: -37.717425, longitude: 145.045857},
        {latitude: -37.717166, longitude: 145.046544},
        {latitude: -37.717242, longitude: 145.046694},
        {latitude: -37.717967, longitude: 145.045865}]
    }, 
	{id: "CP10", name: "Car Park 10", freeSpots: -1, capacity: 42, latitude: -37.714908, longitude: 145.050099, area: [
    	{latitude: -37.714714, longitude: 145.049917},
        {latitude: -37.714786, longitude: 145.050373},
        {latitude: -37.715072, longitude: 145.050328},
        {latitude: -37.715025, longitude: 145.049836}]
	}
];

var motorbikeLocations = [
	{id:"M1", name: "Motorbike Parking", capacity: 14, latitude: -37.720978, longitude: 145.045608},
	{id:"M2", name: "Motorbike Parking", capacity: 12, latitude: -37.720531, longitude: 145.046182},
	{id:"M3", name: "Motorbike Parking", capacity: 6, latitude: -37.724416, longitude: 145.050168}
];

//Ticket Machine Locations
var tmLocations = [
	{id: "TM1", carPark: "CP1", latitude: -37.719715, longitude: 145.044791},//double check location
	{id: "TM2", carPark: "CP2", latitude: -37.722585, longitude: 145.045744},
	{id: "TM3", carPark: "CP2a", latitude: -37.723926, longitude: 145.044115},//cant see them on maps and i cant remember their locations
	{id: "TM4", carPark: "CP3", latitude: -37.725022, longitude: 145.050591},//double check location
	{id: "TM5", carPark: "CP4", latitude: -37.724046, longitude: 145.051652},
	{id: "TM6", carPark: "CP6", latitude: -37.720241, longitude: 145.054926},
	{id: "TM7", carPark: "CP7", latitude: -37.717770, longitude: 145.050771},
	{id: "TM8", carPark: "CP7", latitude: -37.718298, longitude: 145.052339},//this is not an error there are two machines in 7
	{id: "TM9", carPark: "CP8", latitude: -37.717172, longitude: 145.048453},
	{id: "TM10", carPark: "CP9", latitude: -37.717844, longitude: 145.045752}
];

//Locations of all the car park rows
var rowLocations = [
//CP1
	{id: "R101", carPark: "CP1", freeSpots: -1, latitude: -37.719226, longitude: 145.044879},
	{id: "R102", carPark: "CP1", freeSpots: -1, latitude: -37.719191, longitude: 145.044788},
	{id: "R103", carPark: "CP1", freeSpots: -1, latitude: -37.719693, longitude: 145.044607},
	{id: "R104", carPark: "CP1", freeSpots: -1, latitude: -37.720237, longitude: 145.044506},
	{id: "R105", carPark: "CP1", freeSpots: -1, latitude: -37.720783, longitude: 145.044511},
	{id: "R106", carPark: "CP1", freeSpots: -1, latitude: -37.720770, longitude: 145.044636},
	{id: "R107", carPark: "CP1", freeSpots: -1, latitude: -37.720150, longitude: 145.044679},
	{id: "R108", carPark: "CP1", freeSpots: -1, latitude: -37.720150, longitude: 145.044679},
	{id: "R109", carPark: "CP1", freeSpots: -1, latitude: -37.720849, longitude: 145.044731},
//CP2	
	{id: "R201", carPark: "CP2", freeSpots: -1, latitude: -37.721518, longitude: 145.044599},
	{id: "R202", carPark: "CP2", freeSpots: -1, latitude: -37.722034, longitude: 145.044752},
	{id: "R203", carPark: "CP2", freeSpots: -1, latitude: -37.722010, longitude: 145.044885},
	{id: "R204", carPark: "CP2", freeSpots: -1, latitude: -37.721529, longitude: 145.044746}, 
	{id: "R205", carPark: "CP2", freeSpots: -1, latitude: -37.721511, longitude: 145.044835},
	{id: "R206", carPark: "CP2", freeSpots: -1, latitude: -37.722017, longitude: 145.044995},
	{id: "R207", carPark: "CP2", freeSpots: -1, latitude: -37.721955, longitude: 145.045102},
	{id: "R208", carPark: "CP2", freeSpots: -1, latitude: -37.721470, longitude: 145.044979},
	{id: "R209", carPark: "CP2", freeSpots: -1, latitude: -37.721453, longitude: 145.045033},
	{id: "R210", carPark: "CP2", freeSpots: -1, latitude: -37.721881, longitude: 145.045154},
	{id: "R211", carPark: "CP2", freeSpots: -1, latitude: -37.721402, longitude: 145.045173},
	{id: "R212", carPark: "CP2", freeSpots: -1, latitude: -37.721862, longitude: 145.045302},
	{id: "R213", carPark: "CP2", freeSpots: -1, latitude: -37.722677, longitude: 145.045096},
	{id: "R214", carPark: "CP2", freeSpots: -1, latitude: -37.723098, longitude: 145.045566},
	{id: "R215", carPark: "CP2", freeSpots: -1, latitude: -37.723022, longitude: 145.045668},
	{id: "R216", carPark: "CP2", freeSpots: -1, latitude: -37.722622, longitude: 145.045205},
	{id: "R217", carPark: "CP2", freeSpots: -1, latitude: -37.722567, longitude: 145.045256},
	{id: "R218", carPark: "CP2", freeSpots: -1, latitude: -37.722943, longitude: 145.045694},
	{id: "R219", carPark: "CP2", freeSpots: -1, latitude: -37.722867, longitude: 145.045796},
	{id: "R220", carPark: "CP2", freeSpots: -1, latitude: -37.722505, longitude: 145.045394},
	{id: "R221", carPark: "CP2", freeSpots: -1, latitude: -37.722473, longitude: 145.045437},
	{id: "R222", carPark: "CP2", freeSpots: -1, latitude: -37.722836, longitude: 145.045854},
	{id: "R223", carPark: "CP2", freeSpots: -1, latitude: -37.722944, longitude: 145.046217},
//CP2a
	{id: "R2a01", carPark: "CP2a", freeSpots: -1, latitude: -37.723218, longitude: 145.044063},
	{id: "R2a02", carPark: "CP2a", freeSpots: -1, latitude: -37.723315, longitude: 145.044076},
	{id: "R2a03", carPark: "CP2a", freeSpots: -1, latitude: -37.723421, longitude: 145.044134},
	{id: "R2a04", carPark: "CP2a", freeSpots: -1, latitude: -37.723520, longitude: 145.044203}, 
	{id: "R2a05", carPark: "CP2a", freeSpots: -1, latitude: -37.723574, longitude: 145.044241},
	{id: "R2a06", carPark: "CP2a", freeSpots: -1, latitude: -37.723658, longitude: 145.044359},
	{id: "R2a07", carPark: "CP2a", freeSpots: -1, latitude: -37.723761, longitude: 145.044422},
	{id: "R2a08", carPark: "CP2a", freeSpots: -1, latitude: -37.723850, longitude: 145.044480},
	{id: "R2a09", carPark: "CP2a", freeSpots: -1, latitude: -37.723889, longitude: 145.044547},
	{id: "R2a10", carPark: "CP2a", freeSpots: -1, latitude: -37.723958, longitude: 145.044621},
//CP3	
	{id: "R301", carPark: "CP3", freeSpots: -1, latitude: -37.724562, longitude: 145.049012},
	{id: "R302", carPark: "CP3", freeSpots: -1, latitude: -37.724674, longitude: 145.049180},
	{id: "R303", carPark: "CP3", freeSpots: -1, latitude: -37.724842, longitude: 145.049279},
	{id: "R304", carPark: "CP3", freeSpots: -1, latitude: -37.725020, longitude: 145.049346},
	{id: "R305", carPark: "CP3", freeSpots: -1, latitude: -37.725060, longitude: 145.049298},
	{id: "R306", carPark: "CP3", freeSpots: -1, latitude: -37.725144, longitude: 145.049221},
	{id: "R307", carPark: "CP3", freeSpots: -1, latitude: -37.724486, longitude: 145.049911},
	{id: "R308", carPark: "CP3", freeSpots: -1, latitude: -37.724586, longitude: 145.049946},
	{id: "R309", carPark: "CP3", freeSpots: -1, latitude: -37.724650, longitude: 145.049989},
	{id: "R310", carPark: "CP3", freeSpots: -1, latitude: -37.724754, longitude: 145.050040},
	{id: "R311", carPark: "CP3", freeSpots: -1, latitude: -37.724803, longitude: 145.050056},
	{id: "R312", carPark: "CP3", freeSpots: -1, latitude: -37.724920, longitude: 145.050075},
	{id: "R313", carPark: "CP3", freeSpots: -1, latitude: -37.725041, longitude: 145.050091},
	{id: "R314", carPark: "CP3", freeSpots: -1, latitude: -37.725156, longitude: 145.050121},
	{id: "R315", carPark: "CP3", freeSpots: -1, latitude: -37.725213, longitude: 145.050134},
	{id: "R316", carPark: "CP3", freeSpots: -1, latitude: -37.725319, longitude: 145.050142},
	{id: "R317", carPark: "CP3", freeSpots: -1, latitude: -37.724309, longitude: 145.050490},
	{id: "R318", carPark: "CP3", freeSpots: -1, latitude: -37.724404, longitude: 145.050557},
	{id: "R319", carPark: "CP3", freeSpots: -1, latitude: -37.724469, longitude: 145.050592},
	{id: "R320", carPark: "CP3", freeSpots: -1, latitude: -37.724567, longitude: 145.050648},
	{id: "R321", carPark: "CP3", freeSpots: -1, latitude: -37.724618, longitude: 145.050691},
	{id: "R322", carPark: "CP3", freeSpots: -1, latitude: -37.724690, longitude: 145.050769},
	{id: "R323", carPark: "CP3", freeSpots: -1, latitude: -37.724783, longitude: 145.050796},
	{id: "R324", carPark: "CP3", freeSpots: -1, latitude: -37.724872, longitude: 145.050842},
	{id: "R325", carPark: "CP3", freeSpots: -1, latitude: -37.724953, longitude: 145.050890},
	{id: "R326", carPark: "CP3", freeSpots: -1, latitude: -37.725057, longitude: 145.050930},
	{id: "R327", carPark: "CP3", freeSpots: -1, latitude: -37.725110, longitude: 145.050949},
	{id: "R328", carPark: "CP3", freeSpots: -1, latitude: -37.725212, longitude: 145.050992},
	{id: "R329", carPark: "CP3", freeSpots: -1, latitude: -37.724856, longitude: 145.049617},
//CP4
	{id: "R401", carPark: "CP4", freeSpots: -1, latitude: -37.723408, longitude: 145.052290},
	{id: "R402", carPark: "CP4", freeSpots: -1, latitude: -37.723491, longitude: 145.052392},
	{id: "R403", carPark: "CP4", freeSpots: -1, latitude: -37.723728, longitude: 145.051689},
	{id: "R404", carPark: "CP4", freeSpots: -1, latitude: -37.723830, longitude: 145.051775},
	{id: "R405", carPark: "CP4", freeSpots: -1, latitude: -37.723915, longitude: 145.051829},
	{id: "R406", carPark: "CP4", freeSpots: -1, latitude: -37.723975, longitude: 145.051905},
	{id: "R407", carPark: "CP4", freeSpots: -1, latitude: -37.724043, longitude: 145.051940},
	{id: "R408", carPark: "CP4", freeSpots: -1, latitude: -37.723890, longitude: 145.052232},
	{id: "R409", carPark: "CP4", freeSpots: -1, latitude: -37.724003, longitude: 145.052398},
	{id: "R410", carPark: "CP4", freeSpots: -1, latitude: -37.724067, longitude: 145.052457},
	{id: "R411", carPark: "CP4", freeSpots: -1, latitude: -37.723717, longitude: 145.052395},
	{id: "R412", carPark: "CP4", freeSpots: -1, latitude: -37.724256, longitude: 145.051376},
	{id: "R413", carPark: "CP4", freeSpots: -1, latitude: -37.724205, longitude: 145.051617},
	{id: "R414", carPark: "CP4", freeSpots: -1, latitude: -37.724270, longitude: 145.051681},
	{id: "R415", carPark: "CP4", freeSpots: -1, latitude: -37.724340, longitude: 145.051719},
	{id: "R416", carPark: "CP4", freeSpots: -1, latitude: -37.724406, longitude: 145.051770},
	{id: "R417", carPark: "CP4", freeSpots: -1, latitude: -37.724459, longitude: 145.051832},
	{id: "R418", carPark: "CP4", freeSpots: -1, latitude: -37.724521, longitude: 145.051891},
	{id: "R419", carPark: "CP4", freeSpots: -1, latitude: -37.724580, longitude: 145.051961},
	{id: "R420", carPark: "CP4", freeSpots: -1, latitude: -37.724633, longitude: 145.052017},
	{id: "R421", carPark: "CP4", freeSpots: -1, latitude: -37.724695, longitude: 145.052073},
	{id: "R422", carPark: "CP4", freeSpots: -1, latitude: -37.724786, longitude: 145.052137},
	{id: "R423", carPark: "CP4", freeSpots: -1, latitude: -37.724843, longitude: 145.052172},
	{id: "R424", carPark: "CP4", freeSpots: -1, latitude: -37.724915, longitude: 145.052220},
	{id: "R425", carPark: "CP4", freeSpots: -1, latitude: -37.724955, longitude: 145.052303},
	{id: "R426", carPark: "CP4", freeSpots: -1, latitude: -37.724637, longitude: 145.051681},
	{id: "R427", carPark: "CP4", freeSpots: -1, latitude: -37.724868, longitude: 145.052014},
//CP6
	{id: "R601", carPark: "CP6", freeSpots: -1, latitude: -37.720323, longitude: 145.055008},
	{id: "R602", carPark: "CP6", freeSpots: -1, latitude: -37.720319, longitude: 145.054930},
	{id: "R603", carPark: "CP6", freeSpots: -1, latitude: -37.720375, longitude: 145.054800},
	{id: "R604", carPark: "CP6", freeSpots: -1, latitude: -37.720311, longitude: 145.054706},
	{id: "R605", carPark: "CP6", freeSpots: -1, latitude: -37.720765, longitude: 145.054781},
	{id: "R606", carPark: "CP6", freeSpots: -1, latitude: -37.720320, longitude: 145.054572},
	{id: "R607", carPark: "CP6", freeSpots: -1, latitude: -37.720764, longitude: 145.054629},
	{id: "R608", carPark: "CP6", freeSpots: -1, latitude: -37.720318, longitude: 145.054495},
	{id: "R609", carPark: "CP6", freeSpots: -1, latitude: -37.720755, longitude: 145.054557},
	{id: "R610", carPark: "CP6", freeSpots: -1, latitude: -37.720277, longitude: 145.054353},
	{id: "R611", carPark: "CP6", freeSpots: -1, latitude: -37.720765, longitude: 145.054433},
	{id: "R612", carPark: "CP6", freeSpots: -1, latitude: -37.720298, longitude: 145.054269},
	{id: "R613", carPark: "CP6", freeSpots: -1, latitude: -37.720767, longitude: 145.054331},
	{id: "R614", carPark: "CP6", freeSpots: -1, latitude: -37.720720, longitude: 145.054194},
	{id: "R615", carPark: "CP6", freeSpots: -1, latitude: -37.720722, longitude: 145.054130},
	{id: "R616", carPark: "CP6", freeSpots: -1, latitude: -37.720718, longitude: 145.053974},
	{id: "R617", carPark: "CP6", freeSpots: -1, latitude: -37.721635, longitude: 145.055296},
	{id: "R618", carPark: "CP6", freeSpots: -1, latitude: -37.721561, longitude: 145.055172},
	{id: "R619", carPark: "CP6", freeSpots: -1, latitude: -37.721553, longitude: 145.055102},
	{id: "R620", carPark: "CP6", freeSpots: -1, latitude: -37.721536, longitude: 145.054965},
	{id: "R621", carPark: "CP6", freeSpots: -1, latitude: -37.721428, longitude: 145.054847},
	{id: "R622", carPark: "CP6", freeSpots: -1, latitude: -37.721422, longitude: 145.054702},
	{id: "R623", carPark: "CP6", freeSpots: -1, latitude: -37.721403, longitude: 145.054638},
	{id: "R624", carPark: "CP6", freeSpots: -1, latitude: -37.721414, longitude: 145.054491},
	{id: "R625", carPark: "CP6", freeSpots: -1, latitude: -37.721408, longitude: 145.054373},
	{id: "R626", carPark: "CP6", freeSpots: -1, latitude: -37.721395, longitude: 145.054236},
	{id: "R627", carPark: "CP6", freeSpots: -1, latitude: -37.721387, longitude: 145.054158},
	{id: "R628", carPark: "CP6", freeSpots: -1, latitude: -37.721404, longitude: 145.054021},
	{id: "R629", carPark: "CP6", freeSpots: -1, latitude: -37.721861, longitude: 145.055122},
//CP7	
	{id: "R700", carPark: "CP7", freeSpots: -1, latitude: -37.717794, longitude: 145.050222},
	{id: "R701", carPark: "CP7", freeSpots: -1, latitude: -37.717431, longitude: 145.050696},
	{id: "R702", carPark: "CP7", freeSpots: -1, latitude: -37.717516, longitude: 145.050645},
	{id: "R703", carPark: "CP7", freeSpots: -1, latitude: -37.717574, longitude: 145.050621},
	{id: "R704", carPark: "CP7", freeSpots: -1, latitude: -37.717668, longitude: 145.050578},
	{id: "R705", carPark: "CP7", freeSpots: -1, latitude: -37.717711, longitude: 145.050546},
	{id: "R706", carPark: "CP7", freeSpots: -1, latitude: -37.717803, longitude: 145.050472},
	{id: "R707", carPark: "CP7", freeSpots: -1, latitude: -37.717852, longitude: 145.050453},
	{id: "R708", carPark: "CP7", freeSpots: -1, latitude: -37.717943, longitude: 145.050407},
	{id: "R709", carPark: "CP7", freeSpots: -1, latitude: -37.717994, longitude: 145.050388},
	{id: "R710", carPark: "CP7", freeSpots: -1, latitude: -37.718092, longitude: 145.050353},
	{id: "R711", carPark: "CP7", freeSpots: -1, latitude: -37.718137, longitude: 145.050345},
	{id: "R712", carPark: "CP7", freeSpots: -1, latitude: -37.718216, longitude: 145.050200},
	{id: "R713", carPark: "CP7", freeSpots: -1, latitude: -37.717839, longitude: 145.050938},
	{id: "R714", carPark: "CP7", freeSpots: -1, latitude: -37.717732, longitude: 145.051381},
	{id: "R715", carPark: "CP7", freeSpots: -1, latitude: -37.717978, longitude: 145.052096},
	{id: "R716", carPark: "CP7", freeSpots: -1, latitude: -37.717832, longitude: 145.051388},
	{id: "R717", carPark: "CP7", freeSpots: -1, latitude: -37.717887, longitude: 145.051396},
	{id: "R718", carPark: "CP7", freeSpots: -1, latitude: -37.717974, longitude: 145.051409},
	{id: "R719", carPark: "CP7", freeSpots: -1, latitude: -37.718042, longitude: 145.051420},
	{id: "R720", carPark: "CP7", freeSpots: -1, latitude: -37.718127, longitude: 145.051476},
	{id: "R721", carPark: "CP7", freeSpots: -1, latitude: -37.718189, longitude: 145.051481},
	{id: "R722", carPark: "CP7", freeSpots: -1, latitude: -37.718285, longitude: 145.051147},
	{id: "R723", carPark: "CP7", freeSpots: -1, latitude: -37.718257, longitude: 145.051900},
	{id: "R724", carPark: "CP7", freeSpots: -1, latitude: -37.718323, longitude: 145.051911},
	{id: "R725", carPark: "CP7", freeSpots: -1, latitude: -37.718355, longitude: 145.051126},
	{id: "R726", carPark: "CP7", freeSpots: -1, latitude: -37.718442, longitude: 145.051099},
	{id: "R727", carPark: "CP7", freeSpots: -1, latitude: -37.718419, longitude: 145.051853},
	{id: "R728", carPark: "CP7", freeSpots: -1, latitude: -37.718491, longitude: 145.051531},
	{id: "R729", carPark: "CP7", freeSpots: -1, latitude: -37.718587, longitude: 145.051561},
	{id: "R730", carPark: "CP7", freeSpots: -1, latitude: -37.718649, longitude: 145.051564},
	{id: "R731", carPark: "CP7", freeSpots: -1, latitude: -37.718704, longitude: 145.051062},
	{id: "R732", carPark: "CP7", freeSpots: -1, latitude: -37.718522, longitude: 145.052243},
//CP8	
	{id: "R801", carPark: "CP8", freeSpots: -1, latitude: -37.717453, longitude: 145.047903},
	{id: "R802", carPark: "CP8", freeSpots: -1, latitude: -37.717098, longitude: 145.048405},
	{id: "R803", carPark: "CP8", freeSpots: -1, latitude: -37.717215, longitude: 145.048392},
	{id: "R804", carPark: "CP8", freeSpots: -1, latitude: -37.717283, longitude: 145.048362},
	{id: "R805", carPark: "CP8", freeSpots: -1, latitude: -37.717403, longitude: 145.048384},
	{id: "R806", carPark: "CP8", freeSpots: -1, latitude: -37.717516, longitude: 145.048357},
	{id: "R807", carPark: "CP8", freeSpots: -1, latitude: -37.717609, longitude: 145.048319},
	{id: "R808", carPark: "CP8", freeSpots: -1, latitude: -37.717677, longitude: 145.048314},
	{id: "R809", carPark: "CP8", freeSpots: -1, latitude: -37.717775, longitude: 145.048311},
	{id: "R810", carPark: "CP8", freeSpots: -1, latitude: -37.717879, longitude: 145.048311},
	{id: "R811", carPark: "CP8", freeSpots: -1, latitude: -37.717109, longitude: 145.049211},
	{id: "R812", carPark: "CP8", freeSpots: -1, latitude: -37.717217, longitude: 145.049141},
	{id: "R813", carPark: "CP8", freeSpots: -1, latitude: -37.717266, longitude: 145.049120},
	{id: "R814", carPark: "CP8", freeSpots: -1, latitude: -37.717381, longitude: 145.049107},
	{id: "R815", carPark: "CP8", freeSpots: -1, latitude: -37.717498, longitude: 145.049104},
	{id: "R816", carPark: "CP8", freeSpots: -1, latitude: -37.717608, longitude: 145.049056},
	{id: "R817", carPark: "CP8", freeSpots: -1, latitude: -37.717667, longitude: 145.049037},
	{id: "R818", carPark: "CP8", freeSpots: -1, latitude: -37.717777, longitude: 145.049037},
	{id: "R819", carPark: "CP8", freeSpots: -1, latitude: -37.717881, longitude: 145.049040},
	{id: "R820", carPark: "CP8", freeSpots: -1, latitude: -37.717224, longitude: 145.050107},
	{id: "R821", carPark: "CP8", freeSpots: -1, latitude: -37.717279, longitude: 145.049827},
	{id: "R822", carPark: "CP8", freeSpots: -1, latitude: -37.717339, longitude: 145.049800},
	{id: "R823", carPark: "CP8", freeSpots: -1, latitude: -37.717435, longitude: 145.049768},
	{id: "R824", carPark: "CP8", freeSpots: -1, latitude: -37.717557, longitude: 145.049741},
	{id: "R825", carPark: "CP8", freeSpots: -1, latitude: -37.717662, longitude: 145.049722},
	{id: "R826", carPark: "CP8", freeSpots: -1, latitude: -37.717735, longitude: 145.049703},
	{id: "R827", carPark: "CP8", freeSpots: -1, latitude: -37.717825, longitude: 145.049665},
	{id: "R828", carPark: "CP8", freeSpots: -1, latitude: -37.717919, longitude: 145.049630},
	{id: "R829", carPark: "CP8", freeSpots: -1, latitude: -37.717996, longitude: 145.049067},
	{id: "R830", carPark: "CP8", freeSpots: -1, latitude: -37.717765, longitude: 145.050125},
//CP9
	{id: "R901", carPark: "CP9", freeSpots: -1, latitude: -37.717488, longitude: 145.045399},
	{id: "R902", carPark: "CP9", freeSpots: -1, latitude: -37.717561, longitude: 145.045483},
	{id: "R903", carPark: "CP9", freeSpots: -1, latitude: -37.717604, longitude: 145.045553},
	{id: "R904", carPark: "CP9", freeSpots: -1, latitude: -37.717640, longitude: 145.045680},
	{id: "R905", carPark: "CP9", freeSpots: -1, latitude: -37.717666, longitude: 145.045737},
	{id: "R906", carPark: "CP9", freeSpots: -1, latitude: -37.717711, longitude: 145.045854},
	{id: "R907", carPark: "CP9", freeSpots: -1, latitude: -37.717752, longitude: 145.045919},
	{id: "R908", carPark: "CP9", freeSpots: -1, latitude: -37.717822, longitude: 145.046022},
	{id: "R909", carPark: "CP9", freeSpots: -1, latitude: -37.717291, longitude: 145.046276},
	{id: "R910", carPark: "CP9", freeSpots: -1, latitude: -37.717451, longitude: 145.046163},
	{id: "R911", carPark: "CP9", freeSpots: -1, latitude: -37.717464, longitude: 145.046255},
	{id: "R912", carPark: "CP9", freeSpots: -1, latitude: -37.717432, longitude: 145.046449},
//CP10
	{id: "R1001", carPark: "CP10", freeSpots: -1, latitude: -37.714859, longitude: 145.049939},
	{id: "R1002", carPark: "CP10", freeSpots: -1, latitude: -37.714810, longitude: 145.050174},
	{id: "R1003", carPark: "CP10", freeSpots: -1, latitude: -37.714933, longitude: 145.050295},
	{id: "R1004", carPark: "CP10", freeSpots: -1, latitude: -37.714328, longitude: 145.049124},
	{id: "R1005", carPark: "CP10", freeSpots: -1, latitude: -37.714237, longitude: 145.049202}
];

//List of all the buildings and their locations
var academicBuildings = [
	{id: "AB", name: "Agri Bio Building", latitude: -37.723926, longitude: 145.053398},
	{id: "AE", name: "Agora East", latitude: -37.720734, longitude: 145.048812},
	{id: "AGH", name: "Animal Glasshouse Complex", latitude: -37.71815, longitude: 145.047365},
	{id: "AR", name: "Agriculture Reserve", latitude: -37.715468, longitude: 145.047295},
	{id: "AT", name: "Agora Theatre", latitude: -37.721243, longitude: 145.048395},
	{id: "AW", name: "Agora West", latitude: -37.721438, longitude: 145.048905},
	{id: "B", name: "Boiler House", latitude: -37.719593, longitude: 145.054926},
	{id: "BD", name: "Baseball Diamond", latitude: -37.723696, longitude: 145.041406},
	{id: "BG", name: "Beth Gleeson Building", latitude: -37.721414, longitude: 145.046833},
	{id: "BS1", name: "Biological Sciences 1", latitude: -37.719391, longitude: 145.047633},
	{id: "BS2", name: "Biological Sciences 2", latitude: -37.719182, longitude: 145.046882},
	{id: "CC", name: "Chisholm College", latitude: -37.723666, longitude: 145.049891},
	{id: "CCO", name: "Gatehouse", latitude: -37.723859, longitude: 145.047995},
	{id: "CCR", name: "Childrens Centre", latitude: -37.72241, longitude: 145.054692},
	{id: "CS1", name: "Campus Services 1 (I&O)", latitude: -37.719289, longitude: 145.054503},
	{id: "CS2", name: "Campus Services 2", latitude: -37.719349, longitude: 145.054063},
	{id: "CS3", name: "Landscaping", latitude: -37.719281, longitude: 145.053714},
	{id: "CSCN", name: "Cycle Smart Centre North", latitude: -37.719609, longitude: 145.047329},
	{id: "CSCS", name: "Cycle Smart Centre South", latitude: -37.721124, longitude: 145.049711},
	{id: "DMC", name: "David Myers Building- Centre", latitude: -37.722041, longitude: 145.048436},
	{id: "DME", name: "David Myers Building- East", latitude: -37.722084, longitude: 145.048959},
	{id: "DMW", name: "David Myers Building- West", latitude: -37.72205, longitude: 145.047763},
	{id: "DW", name: "Donald Whitehead Building", latitude: -37.720351, longitude: 145.049092},
	{id: "EB", name: "Eagle Bar", latitude: -37.723089, longitude: 145.049767},
	{id: "ED1", name: "Education 1", latitude: -37.721848, longitude: 145.04967},
	{id: "ED2", name: "Education 2", latitude: -37.721568, longitude: 145.049373},
	{id: "ELT", name: "East Lecture Theatre", latitude: -37.720732, longitude: 145.049238},
	{id: "GC", name: "Glenn College", latitude: -37.720734, longitude: 145.051902},
	{id: "GS", name: "George Singer Building", latitude: -37.718947, longitude: 145.047644},
	{id: "HS1", name: "Health Sciences 1", latitude: -37.720265, longitude: 145.045888},
	{id: "HS2", name: "Health Sciences 2", latitude: -37.719871, longitude: 145.045877},
	{id: "HS3", name: "Health Sciences 3", latitude: -37.719183, longitude: 145.045963},
	{id: "HSC", name: "Health Sciences Clinic", latitude: -37.716027, longitude: 145.044736},
	{id: "HSZ", name: "Hooper & Szental Lecture Theatres", latitude: -37.721645, longitude: 145.047009},
	{id: "HU2", name: "Humanities 2", latitude: -37.721354, longitude: 145.049914},
	{id: "HU3", name: "Humanities 3", latitude: -37.721128, longitude: 145.04935},
	{id: "HUED", name: "Education and Humanities", latitude: -37.721572, longitude: 145.049723},
	{id: "ISC", name: "Indoor Sports Centre", latitude: -37.719644, longitude: 145.053128},
	{id: "JSM", name: "John Scott Meeting House", latitude: -37.719589, longitude: 145.051185},
	{id: "L", name: "Library", latitude: -37.720008, longitude: 145.048391},
	{id: "LIMS1", name: "La Trobe Institute for Molecular Science 1", latitude: -37.720107, longitude: 145.046961},
	{id: "LIMS2", name: "La Trobe Institute for Molecular Science 2", latitude: -37.719904, longitude: 145.047573},
	{id: "LMC", name: "La Trobe University Medical Centre", latitude: -37.716564, longitude: 145.044446},
	{id: "LPFP", name: "Lower Playing Field Pavilion", latitude: -37.724728, longitude: 145.041871},
	{id: "MC", name: "Menzies College", latitude: -37.721618, longitude: 145.051756},
	{id: "MT", name: "Moat Theatre", latitude: -37.722479, longitude: 145.050023},
	{id: "NR1", name: "Annexe 1- Institute for Advanced Study", latitude: -37.714266, longitude: 145.050882},
	{id: "NR2", name: "Annexe 2- Institute for Advanced Study", latitude: -37.714574, longitude: 145.050638},
	{id: "NR3", name: "La Trobe Apartments", latitude: -37.714489, longitude: 145.049726},
	{id: "NR4", name: "La Trobe House", latitude: -37.714115, longitude: 145.049488},
	{id: "NR5", name: "Graduate House- West", latitude: -37.713417, longitude: 145.049319},
	{id: "NR6", name: "Centre for Research on Language Diversity", latitude: -37.713683, longitude: 145.049957},
	{id: "NR7", name: "Graduate House- East", latitude: -37.713591, longitude: 145.050453},
	{id: "NR8", name: "Michael J Osborne Centre- Administration", latitude: -37.714239, longitude: 145.05019},
	{id: "PE", name: "Peribolos East", latitude: -37.721538, longitude: 145.048905},
	{id: "PS1", name: "Physical Sciences 1", latitude: -37.721164, longitude: 145.047588},
	{id: "PS2", name: "Physical Sciences 2", latitude: -37.721574, longitude: 145.047417},
	{id: "PW", name: "Peribolos West", latitude: -37.721559, longitude: 145.047914},
	{id: "RLR", name: "R. L. Reid Building", latitude: -37.719643, longitude: 145.046743},
	{id: "SF", name: "Sports Fields", latitude: -37.725462, longitude: 145.041818},
	{id: "SFP", name: "Sports Field Pavilion", latitude: -37.723144, longitude: 145.04227},
	{id: "SS", name: "Social Sciences", latitude: -37.71986, longitude: 145.049039},
	{id: "SW", name: "Sylvia Walton building", latitude: -37.721748, longitude: 145.050151},
	{id: "SWA", name: "South West Annexe", latitude: -37.721839, longitude: 145.046932},
	{id: "TLC", name: "The Learning Commons", latitude: -37.720334, longitude: 145.047582},
	{id: "TC", name: "Thomas Cherry Building", latitude: -37.720765, longitude: 145.047147},
	{id: "U", name: "Union Building", latitude: -37.722937, longitude: 145.050336},
	{id: "UCC", name: "Lifeskills Building", latitude: -37.720559, longitude: 145.047865},
	{id: "WLT", name: "West Lecture Theatres", latitude: -37.719527, longitude: 145.045723},
	{id: "ZA", name: "Zoology Annexe", latitude: -37.7165, longitude: 145.049265},
	{id: "ZR", name: "Zoology Reserve", latitude: -37.71609, longitude: 145.049291}
];